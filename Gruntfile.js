module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-targethtml');
    grunt.loadNpmTasks('grunt-csscomb');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        vendor: grunt.file.readJSON('.bowerrc'),

        less: {
            dev: {
                options: {
                    sourceMap: true,
                    sourceMapURL: '<%= pkg.dir.build.css %>/main.min.css.map',
                    sourceMapFilename: '<%= pkg.dir.build.css %>/main.min.css.map',
                    compress: false,
                    yuicompress: false,
                    strictImports: true
                },

                files: {
                    '<%= pkg.dir.build.css %>/common.css': '<%= pkg.dir.less %>/common.less',
                    '<%= pkg.dir.build.css %>/main.min.tmp.css': '<%= pkg.dir.less %>/main.less'
                }
            },

            build: {
                options: {
                    optimization: 2,
                    compress: true,
                    yuicompress: true,
                    strictImports: true
                },
                files: {
                    '<%= pkg.dir.build.css %>/common.css': '<%= pkg.dir.less %>/common.less',
                    '<%= pkg.dir.build.css %>/main.min.tmp.css': '<%= pkg.dir.less %>/main.less'
                }
            }
        },

        autoprefixer: {
            build: {
                options: {
                    cascade: false,
                    browsers: ['last 2 versions', 'ie 8', 'ie 9']
                },
                src: '<%= pkg.dir.build.css %>/main.min.tmp.css',
                dest: '<%= pkg.dir.build.css %>/main.min.css'
            }
        },

        copy: {
            assets: {
                files: [
                    {
                        src: ['**'],
                        cwd: '<%= pkg.dir.img %>',
                        dest: '<%= pkg.dir.build.img %>/',
                        expand: true
                    },
                    {
                        src: ['*-Icons.*'],
                        cwd: '<%= pkg.dir.fonts %>',
                        dest: '<%= pkg.dir.build.fonts %>/',
                        expand: true
                    }
                ]
            }
        },

        clean: {
            tmp: ['<%= pkg.dir.base %>/**/*.tmp.*'],
            build: ['<%= pkg.dir.build.root %>']
        },

        connect: {
            options: {
                port: 9000,
                open: true,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },

            dev: {
                options: {
                    port: 8002,
                    base: 'public',
                    reload: true
                }
            },

            build: {
                options: {
                    port: 8003,
                    base: 'public/build',
                    keepalive: true
                }
            }
        },

        watch: {
            options: {
                nospawn: true,
                interval: 10000
            },
            less: {
                files: ['<%= pkg.dir.less %>/{,*/}*.less'],
                tasks: ['less:dev', 'autoprefixer', 'csscomb', 'clean:tmp']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= pkg.dir.base %>/{,*/}*.html',
                    '<%= pkg.dir.less %>/{,*/}*.less',
                    '<%= pkg.dir.assets %>/img/{,*/}*'
                ]
            }
        },

        targethtml: {
            build: {
                options: {
                    curlyTags: {
                        random: Math.random().toString(36).replace(/[^a-z]+/g, '')
                    }
                },

                files: {
                    '<%= pkg.dir.build.root %>/index.tmp.html': '<%= pkg.dir.base %>/index.html'
                }
            }
        },

        htmlmin: {
            options: {
                removeComments: true,
                collapseWhitespace: true
            },

            build: {
                files: [{
                    expand: true,
                    src: '*.html',
                    cwd: '<%= pkg.dir.views %>',
                    dest: '<%= pkg.dir.build.views %>/'
                }, {
                    src: '<%= pkg.dir.build.root %>/index.tmp.html',
                    dest: '<%= pkg.dir.build.root %>/index.html'
                }]
            }
        },
        csscomb: {
            server: {
                options: {
                    config: 'csscomb.json'
                },
                files: {
                    '<%= pkg.dir.build.css %>/main.css': ['<%= pkg.dir.build.css %>/main.min.css']
                }
            }
        }
    });

    grunt.registerTask('buildCSS', function (type) {
        type = type === 'dev' ? type : 'build';

        grunt.task.run([
            'less:' + type,
            'autoprefixer',
            'csscomb',
            'clean:tmp'
        ]);
    });

    grunt.registerTask('buildHTML', [
        'targethtml',
        'htmlmin'
    ]);

    grunt.registerTask('build', [
        'clean:build',
        'copy:assets',
        'buildCSS:build',
        'buildHTML',
        'clean:tmp'
    ]);

    grunt.registerTask('default', [
        'buildCSS:dev',
        'connect:dev',
        'watch'
    ]);
};